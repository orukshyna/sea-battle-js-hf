view = {
    displayMessage: function(message) {
        var messageArea = document.getElementById("messageArea");
        messageArea.innerHTML = message;
    },
    displayMiss: function(location) {
        var cell = document.getElementById(location);
        cell.setAttribute("class", "miss");
    },
    displayHit: function(location) {
        var cell = document.getElementById(location);
        cell.setAttribute("class", "hit");
    }
};

var model = {
    boardSize: 7,
    numberOfShips: 3,
    shipLength: 3,
    shipsSunk: 0, // 0 - no sunk ships at the beginning
    ships: [
        { locations: [0, 0, 0], hits: ["", "", ""] },
        { locations: [0, 0, 0], hits: ["", "", ""] },
        { locations: [0, 0, 0], hits: ["", "", ""] },
    ],

    generateShipLocations: function() {
        //realisation: create list of ships with numberOfShips
        var locations;
        for (var i = 0; i < this.numberOfShips; i++) {
            do {
                locations = this.generateShip();
            // if collision returns false then the ship will be generated
            // if true - 'do' statement generate ship again
            } while (this.collision(locations));
            this.ships[i].locations = locations;
        }
    },

    generateShip: function(shipLength) {
        //realisation: create 1 ship
        var direction = Math.floor(Math.random() * 2);
        var row, column;

        if (direction === 1) {
            //generate position for horizontal ship:
            row = Math.floor(Math.random() * this.boardSize);
            column = Math.floor(Math.random() * (this.boardSize - this.shipLength));
        } else {
            //generate position for vertical ship:
            row = Math.floor(Math.random() * (this.boardSize - this.shipLength));
            column = Math.floor(Math.random() * this.boardSize);
        }

        var newShipLocations = [];
        for (var i = 0; i < this.shipLength; i++ ) {
            if (direction === 1) {
                newShipLocations.push(row + "" + (column + i));
            } else {
                newShipLocations.push((row + i) + "" + column);
            }
        }
        return newShipLocations;
    },

    collision: function(locations) {
        //realisation: get 1 ship and check if it is not covered with another ship(s)
        for (var i = 0; i < this.numberOfShips; i++) {
            var ship = model.ships[i];
            for (var j = 0; j < locations.length; j++) {
                if (ship.locations.indexOf(locations[j]) >=0 ) {
                    return true;
                }
            }
        }
        return false;
    },

    fire: function(guess) {
        for (var i = 0; this.numberOfShips > i; i++) {
            var index = this.ships[i].locations.indexOf(guess);
            if (index >= 0) {
                this.ships[i].hits[index] = "hit";
                view.displayHit(guess);
                view.displayMessage("Hit!");
                if (this.isSunk(this.ships[i])) {
                    this.shipsSunk++;
                    view.displayMessage("The battleship was sunk!");
                }
                return true;
            }
        }
        view.displayMiss(guess);
        view.displayMessage("Missed");
        return false;
    },

    isSunk: function(ship) {
        for (var i = 0; i < this.shipLength; i++) {
            if (ship.hits[i] !== "hit") {
                return false;
            }
        }
        return true;
    }
};

var controller = {
    guesses: 0,
    processGuess: function(guess) {
        var location = this.parseGuess(guess);
        if (location) {
            this.guesses++;
            var hit = model.fire(location);
            if (hit && model.shipsSunk === model.numberOfShips) {
                view.displayMessage("All ships were sunk in " + this.guesses + " guesses!");
            }
        }
    },
    parseGuess: function(guess) {
        var letters = ["A", "B", "C", "D", "E", "F", "G"];
        if (guess === null || guess.length !== 2) {
            alert("Enter a number(0-6)/letter(A-G).");
        } else {
            var row = letters.indexOf(guess.charAt(0));
            var column = guess.charAt(1);
            if (isNaN(row) || isNaN(column)) {
                alert("Not on the board.");
            } else if
                (row < 0 || row >= model.boardSize ||
                column < 0 || column >= model.boardSize) {
                    alert("Out of board.");
            } else {
                return row + column;
            }
        }
        return null;
    }
}

function init() {
    var fireButton = document.getElementById("fireButton");
    fireButton.onclick = handleFireButton;
    var guessInput = document.getElementById("guessInput");
    guessInput.onkeypress = handleKeyPress;
    model.generateShipLocations();
}

function handleKeyPress(e) {
    var fireButton = document.getElementById("fireButton");
    if (e.keyCode === 13) {
        fireButton.click();
        return false;
    }
}

function handleFireButton() {
    var guessInput = document.getElementById("guessInput");
    var guess = guessInput.value;
    controller.processGuess(guess);
    guessInput.value = "";
}

window.onload = init;
# Sea Battle JS HF


The project is based on material from the book: 
Head First JavaScript Programming by Eric Freeman

#### Description
Sea Battle is a strategy game where need to guess the locations of all randomly generated ships.
![img.png](img.png)
![img1.png](img1.png)

#### Instruments
The project based on

* JavaScript:
> methods; 
> functions; 
> events;

* HTML;
* CSS.
